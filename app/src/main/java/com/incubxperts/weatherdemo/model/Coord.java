package com.incubxperts.weatherdemo.model;

/**
 * Created by kedar on 6/27/2017.
 * File com.incubxperts.weatherdemo.model
 */

public class Coord {
    double lon;
    double lat;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    @Override
    public String toString() {
        return "Coord{" +
                "lon=" + lon +
                ", lat=" + lat +
                '}';
    }
}