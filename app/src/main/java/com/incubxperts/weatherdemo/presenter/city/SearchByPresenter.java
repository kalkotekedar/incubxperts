package com.incubxperts.weatherdemo.presenter.city;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.incubxperts.weatherdemo.model.Main;
import com.incubxperts.weatherdemo.model.WeatherDetails;
import com.incubxperts.weatherdemo.utility.Utils;
import com.incubxperts.weatherdemo.view.city.ISearchByCity;
import com.incubxperts.weatherdemo.view.city.SearchByCity;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by kedar on 6/27/2017.
 * File com.incubxperts.weatherdemo.presenter.city
 */

public class SearchByPresenter implements ISearchByInterface {

    private static final String TAG = SearchByPresenter.class.getSimpleName();
    private Context context;
    private ISearchByCity iSearchByCity;
    private ProgressDialog mProgressDialog;

    public SearchByPresenter(SearchByCity searchByCity, Context mContext) {
        this.iSearchByCity = searchByCity;
        context = mContext;
    }

    @Override
    public void getWeatherUpdateByCity(String cityName) {
        new getCityForecast().execute(cityName);
    }

    class getCityForecast extends AsyncTask<String, Void, Void> {
        private WeatherDetails mWeatherDetails;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Getting updates please wait...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                OkHttpClient client = new OkHttpClient();

                HttpUrl.Builder urlBuilder = HttpUrl.parse(Utils.URL).newBuilder();
                urlBuilder.addQueryParameter("APPID", Utils.API_KEY);
                urlBuilder.addQueryParameter("units", "metric");
                urlBuilder.addQueryParameter("q", params[0]);
                String url = urlBuilder.build().toString();

                Request request = new Request.Builder()
                        .url(url)
                        .build();
                Log.i(TAG, "doInBackground: URL " + url);
                Response response = client.newCall(request).execute();
                Gson gson = new Gson();
                mWeatherDetails = gson.fromJson(response.body().string(), WeatherDetails.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mProgressDialog.isShowing())
                mProgressDialog.dismiss();
            if (mWeatherDetails != null) {
                if (mWeatherDetails.getCod() == 404) {
                    iSearchByCity.showCityWeatherNull("No city Found");
                } else {
                    Main main = mWeatherDetails.getMain();
                    iSearchByCity.showCityWeather(Double.toString(main.getTemp()), mWeatherDetails.getName());
                }
            }
        }
    }
}
