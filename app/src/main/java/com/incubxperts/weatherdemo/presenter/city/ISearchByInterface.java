package com.incubxperts.weatherdemo.presenter.city;

/**
 * Created by kedar on 6/27/2017.
 * File com.incubxperts.weatherdemo.presenter.city
 */

public interface ISearchByInterface {
    void getWeatherUpdateByCity(String cityName);
}
