package com.incubxperts.weatherdemo.presenter.home;

import android.content.Context;
import android.location.Location;

import com.incubxperts.weatherdemo.model.WeatherDetails;

/**
 * Created by kedar on 6/27/2017.
 * File com.incubxperts.weatherdemo.presenter.home
 */

public interface HomeInterface {
    void getWeatherUpdates(Location location);
}