package com.incubxperts.weatherdemo.presenter.home;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.incubxperts.weatherdemo.model.Main;
import com.incubxperts.weatherdemo.model.WeatherDetails;
import com.incubxperts.weatherdemo.utility.Utils;
import com.incubxperts.weatherdemo.view.home.WDHome;
import com.incubxperts.weatherdemo.view.home.WDInterface;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by kedar on 6/27/2017.
 * File com.incubxperts.weatherdemo.presenter.home
 */

public class HomePresenter implements HomeInterface {
    private static final String TAG = HomePresenter.class.getSimpleName();
    private Context context;
    private WDInterface wdInterface;
    private ProgressDialog mProgressDialog;

    public HomePresenter(WDHome wdHome, Context mContext) {
        this.wdInterface = wdHome;
        this.context = mContext;
    }

    @Override
    public void getWeatherUpdates(Location location) {
        try {
            Geocoder gcd = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (addresses.size() > 0) {
                Log.i(TAG, "getWeatherUpdates: " + addresses.get(0).getLocality());
                new getCityForecast().execute(addresses.get(0).getLocality());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    class getCityForecast extends AsyncTask<String, Void, Void> {
        private WeatherDetails mWeatherDetails;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Getting updates please wait...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                OkHttpClient client = new OkHttpClient();

                HttpUrl.Builder urlBuilder = HttpUrl.parse(Utils.URL).newBuilder();
                urlBuilder.addQueryParameter("APPID", Utils.API_KEY);
                urlBuilder.addQueryParameter("units", "metric");
                urlBuilder.addQueryParameter("q", params[0]);
                String url = urlBuilder.build().toString();

                Request request = new Request.Builder()
                        .url(url)
                        .build();
                Log.i(TAG, "doInBackground: URL " + url);
                Response response = client.newCall(request).execute();
                Gson gson = new Gson();
                mWeatherDetails = gson.fromJson(response.body().string(), WeatherDetails.class);
                Log.i(TAG, "doInBackground: " + mWeatherDetails.getWeather().toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mProgressDialog.isShowing())
                mProgressDialog.dismiss();
            if (mWeatherDetails != null) {
                Main main = mWeatherDetails.getMain();
                wdInterface.displayWeatherDetails(Double.toString(main.getTemp()), mWeatherDetails.getName());
            }
        }
    }
}