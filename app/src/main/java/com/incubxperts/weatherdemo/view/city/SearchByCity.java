package com.incubxperts.weatherdemo.view.city;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.incubxperts.weatherdemo.R;
import com.incubxperts.weatherdemo.presenter.city.ISearchByInterface;
import com.incubxperts.weatherdemo.presenter.city.SearchByPresenter;

public class SearchByCity extends AppCompatActivity implements ISearchByCity {

    private Context mContext = this;
    private ISearchByInterface iSearchByInterface;
    TextView TVTemprature, TVLocation;
    EditText ETGetCityName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_by_city);

        TVTemprature = (TextView) findViewById(R.id.tv_temprature);
        TVLocation = (TextView) findViewById(R.id.tv_location);
        ETGetCityName = (EditText) findViewById(R.id.et_city_name);

        iSearchByInterface = new SearchByPresenter(this, mContext);
    }

    public void getWeatherUpdates(View view) {
        if (!ETGetCityName.getText().toString().equals(""))
            iSearchByInterface.getWeatherUpdateByCity(ETGetCityName.getText().toString());
    }

    @Override
    public void showCityWeather(String temp, String location) {
        TVTemprature.setText(temp + (char) 0x00B0);
        TVLocation.setText(location);
    }

    @Override
    public void showCityWeatherNull(String message) {
        TVTemprature.setText("");
        TVLocation.setText(message);
    }
}