package com.incubxperts.weatherdemo.view.city;

/**
 * Created by kedar on 6/27/2017.
 * File com.incubxperts.weatherdemo.presenter.city
 */

public interface ISearchByCity {
    void showCityWeather(String temp, String location);

    void showCityWeatherNull(String message);
}
