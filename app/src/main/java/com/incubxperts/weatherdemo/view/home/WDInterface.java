package com.incubxperts.weatherdemo.view.home;

/**
 * Created by kedar on 6/27/2017.
 * File com.incubxperts.weatherdemo.view.home
 */

public interface WDInterface {
    void displayWeatherDetails(String temp, String name);
}