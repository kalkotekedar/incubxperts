package com.incubxperts.weatherdemo.view.splashscreen;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.incubxperts.weatherdemo.BaseActivity;
import com.incubxperts.weatherdemo.R;
import com.incubxperts.weatherdemo.view.home.WDHome;

public class SplashScreen extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext(), WDHome.class));
                finish();
            }
        }, 2000);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}